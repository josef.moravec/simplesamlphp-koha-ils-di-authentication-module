SimpleSAMLphp Koha ILS-DI authentication module
===============================================

Install using git
-----------------
```
cd simplesamlphp/modules
git clone authkohailsdi https://gitlab.com/josef.moravec/simplesamlphp-koha-ils-di-authentication-module.git
touch authkohailsdi/enable
```

Install using composer
----------------------
```
composer require josef.moravec/simplesamlphp-module-authkohailsdi
touch vendor/josef.moravec/simplesamlphp-module-authkohailsdi/enable
```

Configure
--------
Add this to your config/authsources.php file:
```
$config = [

    'kohailsdi' => [
        'authkohailsdi:KohaILSDI',
        'ilsdi_api_url' => 'https://<your-library-catalog.org>/cgi-bin/koha/ilsdi.pl',
        'default_affiliation' => ['member', 'student' ],
        'affiliation_mapping' => [
            '<PATRONCATEGORY>' => ['member', 'employee' ],
        ],
        'domain' => '<library.org>',
	'agencyId' => '<sigla>',
        'organizationName' => '<library name>'

    ],
];
```

