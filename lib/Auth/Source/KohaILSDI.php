<?php

namespace SimpleSAML\Module\authkohailsdi\Auth\Source;

use SimpleSAML\Module;

/**
 * Authenticate using Koha ILS-DI API
 *
 * @author Josef Moravec
 * @package SimpleSAMLphp
 */

class KohaILSDI extends \SimpleSAML\Module\core\Auth\UserPassBase
{

    protected $defaultAffiliation;

    protected $affiliationMapping;

    protected $apiUrl;

    protected $domain;

    protected $agencyId;

    protected $organizationName;

    /**
     * Constructor for this authentication source.
     *
     * @param array $info  Information about this authentication source.
     * @param array $config  Configuration.
     */
    public function __construct($info, $config)
    {
        assert(is_array($info));
        assert(is_array($config));

        // Call the parent constructor first, as required by the interface
        parent::__construct($info, $config);

        $this->apiUrl = $config['ilsdi_api_url'];
        $this->affiliationMapping = $config['affiliation_mapping'];
        $this->defaultAffiliation = $config['default_affiliation'];
        $this->domain = $config['domain'];
        $this->agencyId = $config['agencyId'];
        $this->organizationName = $config['organizationName'];
    }

    /**
     * Log-in using Koha ILS-DI API
     *
     * @param array &$state  Information about the current authentication.
     */

    protected function login($username, $password)
    {
        $request = "AuthenticatePatron" . "&username="
            . urlencode($username) . "&password=" . urlencode($password);
        $idObj = $this->makeRequest($request);
        $id = $idObj->{'id'};
        if ($idObj && $idObj->{'code'} && $idObj->{'code'} == 'NotAllowed') {
            throw new \SimpleSAML\Error\Error('This server does not have access to ILS-DI API');
        }
        if ($id) {
            $rsp = $this->makeRequest(
                "GetPatronInfo&patron_id=$id&show_contact=1"
            );
            \SimpleSAML\Logger::debug(var_export($this->affiliationMapping,true));
            $eduPersonAffiliation = $this->defaultAffiliation;
            if (isset($this->affiliationMapping[(string)$rsp->{'categorycode'}])
                && is_array($this->affiliationMapping[(string)$rsp->{'categorycode'}])
            ) {
                $eduPersonAffiliation = $this->affiliationMapping[(string)$rsp->{'categorycode'}];
            }
            if ((string)$rsp->{'is_expired'} === '1') {
                $eduPersonAffiliation = ['library-walk-in'];
            }
            $commonname = $rsp->{'firstname'} . " " . $rsp->{'surname'};
            setlocale(LC_CTYPE, locale_get_default());
            $profile = [
                'cn' => [ $commonname ],
                'eduPersonPrincipalName' => [ $this->addScope($rsp->{'cardnumber'}) ],
                'eduPersonScopedAffiliation' => $this->addScope($eduPersonAffiliation),
                'eduPersonUniqueId' => [$this->addScope($idObj->{'id'})],
                'unstructuredName' => [(string)$idObj->{'id'}],
                'givenName' => [(string)$rsp->{'firstname'}],
                'mail' => [(string)$rsp->{'email'}],
                'sn' => [(string)$rsp->{'surname'}],
                'uid' => [(string)$idObj->{'id'}],
                'displayName' => [$rsp->{'firstname'} . " " . $rsp->{'surname'}],
                'eduPersonAffiliation' => $eduPersonAffiliation,
                'userLibraryId' => [(string)$idObj->{'id'}],
                'userHomeLibrary' =>  [$this->agencyId],
                'o' => [$this->organizationName],
                'eduPersonEntitlement' => ['urn:mace:dir:entitlement:common-lib-terms'],
                'telephoneNumber' => [(string)$rsp->{'phone'}],
                'commonNameASCII' => [iconv("UTF-8", "ASCII//TRANSLIT", $commonname)],
                'schacHomeOrganization' => [$this->domain],
            ];
            \SimpleSAML\Logger::debug(var_export($profile, true));
            return $profile;
        } else {
            throw new \SimpleSAML\Error\Error('WRONGUSERPASS');
        }
    }

    /**
     * Make Request
     *
     * Makes a request to the Koha ILSDI API
     *
     * @param string $api_query   Query string for request
     * @param string $http_method HTTP method (default = GET)
     *
     * @throws ILSException
     * @return obj
     */
    protected function makeRequest($api_query)
    {
        $url = $this->apiUrl . "?service=" . $api_query;
        $http_headers = [
            "Accept: text/xml",
            "Accept-encoding: plain",
        ];
        try {
            $client = curl_init($url);
            curl_setopt($client, CURLOPT_HTTPHEADER, $http_headers );
            curl_setopt($client, CURLOPT_RETURNTRANSFER , true );
            $result = curl_exec($client);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        if (!$result) {
            throw new \Exception('HTTP error' . curl_error($client));
        }
        $resultxml = simplexml_load_string($result);
        if (!$resultxml) {
            throw new \Exception(
                "XML is not valid, URL: $url method: $http_method answer: $result."
            );
        }
        return $resultxml;
    }

    protected function addScope( $var )
    {
        if ( is_array($var) ) {
            return array_map(function ($val) { return $val . "@" . $this->domain; }, $var);
        }
        return $var . "@" . $this->domain;
    }
}
